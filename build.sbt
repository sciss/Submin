lazy val baseName         = "Submin"
lazy val projectVersion   = "0.3.6"
lazy val authorName       = "Hanns Holger Rutz"
lazy val authorEMail      = "contact@sciss.de"
lazy val gitRepoHost      = "codeberg.org"
lazy val gitRepoUser      = "sciss"
lazy val gitRepoName      = baseName

ThisBuild / version       := projectVersion
ThisBuild / versionScheme := Some("pvp")

lazy val root = project.in(file("."))
  .settings(publishSettings)
  .settings(
    name                      := baseName,
    organization              := "de.sciss",
    licenses                  := Seq("GPL v3+" -> url("https://www.gnu.org/licenses/gpl-3.0.txt")),
    scalaVersion              := "2.13.12", // not used
    autoScalaLibrary          := false,
    crossPaths                := false,
    homepage                  := Some(url(s"https://$gitRepoHost/$gitRepoUser/$gitRepoName")),
    libraryDependencies ++= Seq(
      "com.weblookandfeel" % "weblaf-core"     % deps.main.weblaf,
      "com.weblookandfeel" % "weblaf-ui"       % deps.main.weblaf,
      "com.fifesoft"       % "rsyntaxtextarea" % deps.test.rsyntax % Test
    ),
    javacOptions              := commonJavaOptions ++ Seq("-target", "1.8", "-g", "-Xlint:deprecation" /*, "-Xlint:unchecked" */),
    doc / javacOptions        := commonJavaOptions,
    Test / run / fork         := true,
    Test / run / mainClass    := Some("de.sciss.submin.SubminStyleEditor"),
    Test / run / javaOptions ++= openJavaOptions,
  )

lazy val deps = new {
  val main = new {
    val weblaf  = "1.2.14"
  }
  val test = new {
    val rsyntax = "3.1.6"
  }
}

def commonJavaOptions = Seq("-source", "1.8")

// cf. https://github.com/mgarin/weblaf#java-9
lazy val openJavaOptions = Seq(
  "java.base/java.lang",
  "java.base/java.lang.reflect",
  "java.base/java.net",
  "java.base/java.text",
  "java.base/java.util",
  "java.desktop/com.apple.laf",
  "java.desktop/com.sun.java.swing.plaf.gtk",
  "java.desktop/com.sun.java.swing.plaf.windows",
  "java.desktop/java.awt",
  "java.desktop/java.awt.font",
  "java.desktop/java.awt.geom",
  "java.desktop/java.beans",
  "java.desktop/javax.swing.plaf.basic",
  "java.desktop/javax.swing.plaf.synth",
  "java.desktop/javax.swing",
  "java.desktop/javax.swing.table",
  "java.desktop/javax.swing.text",
  "java.desktop/sun.awt",
  "java.desktop/sun.font",
  "java.desktop/sun.swing",
  "java.desktop/sun.swing.table",
).map { pkg => s"--add-opens=$pkg=ALL-UNNAMED" }

// ---- publishing to Maven Central ----

lazy val publishSettings = Seq(
  publishMavenStyle := true,
  Test / publishArtifact := false,
  pomIncludeRepository := { _ => false },
  developers := List(
    Developer(
      id    = "sciss",
      name  = "Hanns Holger Rutz",
      email = "contact@sciss.de",
      url   = url("https://www.sciss.de")
    )
  ),
  scmInfo := {
    Some(ScmInfo(
      url(s"https://$gitRepoHost/$gitRepoUser/$baseName"),
      s"scm:git@$gitRepoHost:$gitRepoUser/$baseName.git"
    ))
  }
)
