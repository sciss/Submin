package de.sciss.submin;

import javax.swing.*;
import java.awt.*;

public class FileChooserTest implements Runnable {
    private final boolean dir;
    private final boolean save;

    public static void main(String[] args) {
        final boolean dir  = args.length > 0 && args[0].equals("--directory");
        final boolean save = args.length > 0 && args[0].equals("--save");
        EventQueue.invokeLater(new FileChooserTest(dir, save));
    }

    public FileChooserTest(boolean dir, boolean save) {
        this.dir    = dir;
        this.save   = save;
    }

    @Override
    public void run() {
        Submin.install(false);
        final JFileChooser fc = new JFileChooser();
        if (dir) fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if (save) fc.showSaveDialog(null); else fc.showOpenDialog(null);
    }
}
